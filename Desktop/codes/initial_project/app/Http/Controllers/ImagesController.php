<?php

namespace App\Http\Controllers;

use App\Rules\ImageRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ImagesController extends Controller
{
    public function save(Request $request)
    {
         $validator = $request->validate([
             'file'=>['required','image','mimes:jpeg,jpg,png' , new ImageRule()],
         ]);

         $request->file('file')->storeAs('/images' , $request->file('file')->getClientOriginalName());

    }
}
