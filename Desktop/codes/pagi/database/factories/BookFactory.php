<?php

namespace Database\Factories;

use App\Models\book;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    protected $model = book::class ;

    public function definition()
    {
        return [
            'name' => $this->faker->colorName() . " " . $this->faker->firstNameFemale(),
            'writer' => $this->faker->firstName() . ' ' . $this->faker->lastName(),
            'text' => $this->faker->sentence(15),
            'price' => $this->faker->numberBetween(2,100)
        ];
    }
}
