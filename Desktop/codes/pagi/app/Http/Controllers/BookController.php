<?php

namespace App\Http\Controllers;

use App\Models\book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public static function add_Pages($pages , $first , $last)
    {
        for ($i=$first ; $i<=$last ; $i++)
        {
            $pages[] = $i;
        }
        return $pages;
    }

    public static function create($items , $each_side_count)
    {
        $last = $items->lastPage();
        $current = $items->currentPage();

        $each_side = $each_side_count;

        $pages = [1];

        if($current-($each_side+2)>0)
        {
            $pages[] = '.';
            $pages = self::add_Pages($pages , $current-$each_side , $current-1);
        }
        else
        {
            $pages = self::add_Pages($pages , 2 , $current-1);
        }

        if($current!=1)
        {
            $pages [] = $current;
        }

        if($last-($each_side+$current+1)>0)
        {
            $pages = self::add_Pages($pages , $current+1 , $current+$each_side);
            $pages[] = '.';
            $pages[] = $last;
        }
        else
        {
            $pages = self::add_Pages($pages , $current+1 , $last);
        }

        return ['items' => $items, 'pages' => $pages , 'current' => $current];

    }

    public function test()
    {
        $books = book::paginate(12);

        return view('welcome' ,
            array_merge(['books' => $books] , self::create($books , 3))
        );
    }

}
