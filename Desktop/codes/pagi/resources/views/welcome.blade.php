<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/app.css')}}">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">

            <table class="border-collapse border border-gray-200 text-center mx-auto mt-4">
                <thead>
                    <tr>
                        <th class="border border-slate-300 py-3 px-8">Name</th>
                        <th class="border border-slate-300 py-3 px-8">Writer</th>
                        <th class="border border-slate-300 py-3 px-8">Text</th>
                        <th class="border border-slate-300 py-3 px-8">Price</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($books as $book)
                        <tr>
                            <td class="border border-slate-300 py-2 px-4">{{$book->name}}</td>
                            <td class="border border-slate-300 py-2 px-4">{{$book->writer}}</td>
                            <td class="border text-left border-slate-300 py-2 px-4">{{$book->text}}</td>
                            <td class="border border-slate-300 py-2 px-4">{{$book->price . "$"}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

    @include('paginate')

    </body>
</html>
